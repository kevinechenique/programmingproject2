package spellingBeeGame.spellingbee.client;

public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	
	private final String sevenLetters = "gifroev";//in order says forgive, can also take other words form this combination too
	private final char centerLetter = 'o';
	private int totalPoints = 14;
	private int score;
	
	//since we already defined the values of the above fields we'll use the default constructor (no constructor written)
	
	@Override
	public int getPointsForWords(String attempt) {
		if(attempt.length() == 4) {
			return 1;
		}
		else if(attempt.equals("forgive")) {
			return 14;
		}
		else if(attempt.length() > 4) {
			return attempt.length();
		}
		return 0;
	}
	@Override
	public String getMessage(String attempt) {
		if(attempt.length() >= 4) {
			return "Good job!";
		}
		else if(attempt.length() < 4);{
			return "Too short";
		}
		
	}
	@Override
	public String getAllLetters() {
		return this.sevenLetters;
	}
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}
	@Override
	public int getScore() {
		return this.score;
	}
	@Override
	public int[] getBrackets() {
		int[] scores = new int[5];
        scores[0]=50;
        scores[1]=100;
        scores[2]=150;
        scores[3]=180;
        scores[4]=200;
        return scores;
	}
	

}
