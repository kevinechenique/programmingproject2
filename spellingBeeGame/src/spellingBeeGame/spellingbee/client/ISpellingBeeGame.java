package spellingBeeGame.spellingbee.client;

/**
 * Contains all the methods that have to do with storing a spelling bee game, as well as evaluation words.
 * It will have to keep track of the letters chosen as well as the list of words used for the dictionary 
 */
public interface ISpellingBeeGame {
	
	//Returns an amount of points based on the word chosen
	int getPointsForWords(String attempt);
	//Check whether a word is valid or not. It returns a message based on the reason the word is rejected or a positive message
	String getMessage(String attempt);
	//Returns a set of 7 letters as a String that the spelling bee object is storing
	String getAllLetters();
	//Return the center character (The character that is required to be part of every word
	char getCenterLetter();
	//Returns the current score of the user
	int getScore();
	//Used in the GUI and determines the various point categories (whatever that means)
	int[] getBrackets();
}
