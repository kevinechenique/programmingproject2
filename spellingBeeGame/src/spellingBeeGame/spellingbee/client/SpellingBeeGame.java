package spellingBeeGame.spellingbee.client;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.io.IOException;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//Stores all the information necessary for a game
public class SpellingBeeGame implements ISpellingBeeGame{
	
	//Contains the seven letters (from given text file)
		private String letters;
		private char centerLetter;
		private int score; 
		//Stores the words found by the player (chosen because order matters)
		private ArrayList<String> wordsFound = new ArrayList<String>();
		//Stores the overall set of possible words (English file)
		private static ArrayList<String> possibleWords = createWordsFromFile("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project2\\project_2\\programmingproject2\\spellingBeeGame\\datafiles\\english.txt");
		//Stores the contents of the letterCombinations file
		private ArrayList<String> letterCombi = createWordsFromFile("C:\\Users\\kevin\\Documents\\DAWSON Documents\\5th Semester\\Programming 3\\Projects\\Project2\\project_2\\programmingproject2\\spellingBeeGame\\datafiles\\letterCombinations.txt"); 
		
	//CONSTRUCTORS
	public SpellingBeeGame() {
		Random rand = new Random();
		//gives an int representing any possible set of seven letters
		int randSet = rand.nextInt(this.letterCombi.size() + 1);//+1 since value is not inclusive
		//gives an int representing the middle char from a set of 7 letters
		int randLetter = rand.nextInt(8);
		
		//gets a random set of 7 words
		this.letters = letterCombi.get(randSet);
		//get a random char from a 7 word set (this will be our center letter)
		this.centerLetter = letters.charAt(randLetter);
		
		//the rest of the fields were left at default
		
	}
	public SpellingBeeGame(String sevenCharacters) {
		Random rand = new Random();
		int randSet = rand.nextInt(8);
		this.letters = sevenCharacters;
		this.centerLetter = letters.charAt(randSet);
		
	}
	
	//METHODS
	public static ArrayList<String> createWordsFromFile(String path){
		
		ArrayList<String> words = new ArrayList<String>();
		try {
		Path p = Paths.get(path);
		List<String> lines = Files.readAllLines(p);
		
		for(String s : lines) {
			words.add(s);
		}
		}
		catch(Exception e) {
			
		}
		return words;
		
	}
	
	@Override
	public int getPointsForWords(String word) {//word represents a word from the english file
			
			int points = 0;
			if(word.length() < 4) {
				points = 0;
			}
			else if(word.length() == 4) {
				points = 1;
			}
			else if(useAllLetters(word)) {//useAllLetter is a boolean method (to be done) that returns true when all letters in a set of 7 letters are used
				points = word.length() + 7;
				return points;
			}
			else if(word.length() > 4) {
				points = word.length();
			}
			return points;
	}
	//gives a message why the word was rejected or a positive message if it was accepted
	@Override
	public String getMessage(String attempt) {
		if(isValidWord(attempt)) {
			return "Good job!";
		}
		else if(!(isValidWord(attempt))) {
			return "Your word is not valid";
		}
		else if(attempt.length() < 4);{
			return "Too short";
		}
		/*else if(//condition goes here) {
			return "The middle letter was not used";
		}*/
	}
	@Override
	public String getAllLetters() {
		return this.letters;
	}
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}
	@Override
	public int getScore() {
		return score;
	}
	
	//Calculates the maximum number of points that a player can get using a set of 7 letters (how many words can a user make using the set of 7 letter)
	@Override
	public int[] getBrackets() {
		
		int[] values = new int[5];
		int wordPoints = 0;
		for(int i=0; i<SpellingBeeGame.possibleWords.size(); i++) {
			if(isValidWord(SpellingBeeGame.possibleWords.get(i))) {
				wordPoints += getPointsForWords(SpellingBeeGame.possibleWords.get(i));
			}
		}
		values[0] = (int) (wordPoints*0.25);
		values[1] = (int) (wordPoints*0.50);
		values[2] = (int) (wordPoints*0.75);
		values[3] = (int) (wordPoints*0.90);
		values[4] = (int) (wordPoints);
		
		return values;
		
	}
	
	//HELPER METHODS
	
	//checks if the word matches the set of seven given letters 
	public boolean isValidWord(String sentence) {
		
		int matches = 0;//counts the number of words matching the given word set
		for(int i=0;i<sentence.length(); i++) {
	        for(int j=0; j<this.letters.length(); j++) {
	            if (sentence.charAt(i)== this.letters.charAt(j)){
	                matches++;
	            }
	        }
	    }
		if(matches == sentence.length()) {
		return true;
		}
		return false;
	}
	//Detects if all 7 letters form the set of letters has been used
	public boolean useAllLetters(String sentence) {
		
		int matches = 0;
		ArrayList<String> setWords = new ArrayList<String>();
		ArrayList<String> sentenceWords = new ArrayList<String>();
		
		//Storing sentence chars into sentenceWords
		for(int i=0; i<sentence.length(); i++) {
			char currentChar = sentence.charAt(i);
			String singleLetter = Character.toString(currentChar);
			sentenceWords.add(singleLetter);
		}
		//Storing given set words into setWords
		for(int i=0; i<this.letters.length(); i++) {
			char currentChar = this.letters.charAt(i);
			String singleLetter = Character.toString(currentChar);
			setWords.add(singleLetter);
		}
		//Determines whether all 7 letters are used to form a word or not
		if(sentence.length() >= 7) {
			for(int i=0; i<this.letters.length(); i++) {
				for(int j=0; j<sentence.length();j++) {
					if(sentenceWords.get(j).contains(setWords.get(i))) {
						matches++;
					}
				}
			}
		}
		if(matches == 7) {
			return true;
		}
		return false;
		
		
	}

	
	
	

}
