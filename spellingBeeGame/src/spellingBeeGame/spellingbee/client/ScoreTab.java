package spellingBeeGame.spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class ScoreTab extends Tab{
	
	//used to interact with the server
	//Client client;
	
	private SpellingBeeGame sbg = new SpellingBeeGame();
	
	private int[] brackets = new int[5];
	
	//For numbers
	private TextField tf1;
	private TextField tf2;
	private TextField tf3;
	private TextField tf4;
	private TextField tf5;
	private TextField tf6;
	
	//For letters
	private TextField tf7 = new TextField("King of the Hill");
	private TextField tf8 = new TextField("IQ Over 9000");
	private TextField tf9 = new TextField("Amazing");
	private TextField tf10 = new TextField("good");
	private TextField tf11 = new TextField("okay");
	private TextField tf12 = new TextField("Current Score");
	
	public ScoreTab(/*Client client*/) {
		super("Score");
		//this.client = client;
		this.brackets = this.sbg.getBrackets();
		this.tf1 = new TextField();
		
		
	}

}
