package spellingBeeGame.spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class GameTab extends Tab{
	
	private Button btn1;
	private Button btn2;
	private Button btn3;
	private Button btn4;
	private Button btn5;
	private Button btn6;
	private Button btn7;
	
	public GameTab(String b1, String b2, String b3, String b4, String b5, String b6, String b7) {
		super("Game");

		this.btn1 = new Button(b1);
		this.btn2 = new Button(b2);
		this.btn3 = new Button(b3);
		this.btn4 = new Button(b4);
		this.btn5 = new Button(b5);
		this.btn6 = new Button(b6);
		this.btn7 = new Button(b7);
		
	}

}
